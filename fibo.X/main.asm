.include "p24ep256mc202.inc"
.global __reset
.bss
    VECT: .sapce 10*2
.text
--reset:
    mov #VECT,w4
    mov #1,w0
    mov #1,w1
    mov #10,w2
FIB1:
    add w0,w1,w3
    mov w3,[w4++]
    mov w0,w1
    mov w3,w0
    dec w2,w2
    bra NF,FIB1
STOP:
    bra STOP
    


